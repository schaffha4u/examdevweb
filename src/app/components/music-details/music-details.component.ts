import { MusicService } from 'src/app/services/music.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Music } from 'src/app/models/music';

@Component({
  selector: 'app-music-details',
  templateUrl: './music-details.component.html',
  styleUrls: ['./music-details.component.css']
})
export class MusicDetailsComponent implements OnInit{
  id: string;
  music!: Music;

  constructor(private _Activatedroute:ActivatedRoute, private musicService: MusicService){
    this.id = this._Activatedroute.snapshot.params["id"];
  }

  ngOnInit(): void {
    this.musicService.getById(this.id).subscribe(result => this.music = result);
  }

}
