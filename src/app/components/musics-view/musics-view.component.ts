import { MusicService } from '../../services/music.service';
import { Music } from '../../models/music';
import { Component, OnInit} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MusicDialogComponent } from '../music-dialog/music-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-musics-view',
  templateUrl: './musics-view.component.html',
  styleUrls: ['./musics-view.component.css']
})
export class MusicsViewComponent implements OnInit {
  displayedColumns: string[] = ['picture', 'title', 'description', 'album', 'artist', 'date', 'style', 'update', 'delete'];
  dataSource: MatTableDataSource<Music> = new MatTableDataSource<Music>([]);

  searchTerm: string = "";

  view: 'list' | 'card';

  constructor(private musicService: MusicService, private dialog:MatDialog, private router: Router) {
    this.view = 'list';
  }

  ngOnInit() {
    this.getAllMusics();
  }

  openDialog(musicId:string|null = null) {
    const dialogRef = this.dialog.open(MusicDialogComponent, (
      {
        data: musicId == null ? {} : {musicId: musicId},
        width: '600px'
      }
    ));
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const resultMusic: Music = result.music;
        if (musicId === null)
          this.musicService.create(resultMusic).subscribe(_ => this.getAllMusics());
        else{
          resultMusic.id = musicId;
          this.musicService.update(resultMusic).subscribe(_ => this.getAllMusics());
        }
        console.log(resultMusic)
      }
    });
  }

  searchByTerm(){
    if (this.searchTerm.length == 0) this.getAllMusics();
    else
      this.musicService.searchByName(this.searchTerm).subscribe(
        musics => this.dataSource.data = musics
      );
  }

  addMusic() {
    this.openDialog();
  }

  editMusic(musicId:string) {
    this.openDialog(musicId);
  }

  getAllMusics() {
    this.musicService.getAll().subscribe(
      musics => this.dataSource.data = musics
    );
  }

  deleteMusic(musicId: string){
    this.musicService.delete(musicId).subscribe();
    this.getAllMusics();
  }

  switchView() {
    this.view = (this.view == 'list') ? 'card' : 'list';
  }

  randomMusic(){
    const id: string = this.dataSource.data[Math.floor(Math.random() * this.dataSource.data.length)].id ?? "-1";
    this.router.navigateByUrl('/music/' + id);
  }
}
