import { Component, ElementRef, Inject, Injector, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Music } from 'src/app/models/music';
import { MusicService } from 'src/app/services/music.service';
@Component({
  selector: 'app-music-dialog',
  templateUrl: './music-dialog.component.html',
  styleUrls: ['./music-dialog.component.css']
})
export class MusicDialogComponent implements OnInit {
  dialogType: "create" | "update";
  form: FormGroup;

  currentMusicId!: string;
  music!: Music;

  readonly separatorKeysCodes = [13, 188] as const;

  constructor(private dialogRef: MatDialogRef<MusicDialogComponent>,
              private musicService: MusicService,
              @Inject(MAT_DIALOG_DATA) public data: any) {
    this.form = MusicDialogComponent.buildForm();
    this.currentMusicId = data?.musicId;
    this.dialogType = this.currentMusicId == null ? "create" : "update";
    console.log(this.dialogType)
    this.music = {
      styles: []
    };
  }

  ngOnInit(): void {
    if (this.dialogType == "update" && this.currentMusicId !== null){
      this.musicService.getById(this.currentMusicId).subscribe(result => {
        this.music = result;
        this.form.patchValue({
          title: this.music.title,
          description: this.music.description,
          album: this.music.album,
          artist: this.music.artist,
          duration: this.music.duration,
          date: this.music.date,
          styles: this.music.styles || [],
          picture: this.music.picture
        })
      });
    }
  }

  onFileSelected(event: any){
    const files = (<HTMLInputElement>event?.currentTarget).files;
    const file:File | null = files!.item(0);

    if (file) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = (_event) => {
          this.music.picture = reader.result;
        }
    }
  }

  validate(music: Music): void{
    music.styles = this.music.styles;
    music.picture = this.music.picture;
    this.dialogRef.close({music: music});
  }

  cancel(): void{
    console.log(this.form)
    //this.dialogRef.close();
  }

  addChipset(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.music.styles!.push(value);
    }
    event.chipInput!.clear();
  }

  removeChipset(style: any): void {
    const index = this.music.styles!.indexOf(style);
    this.music.styles!.splice(index, 1);
  }

  /**
   * Fonction pour construire notre formulaire
   * @returns {FormGroup}
   *
   * @private
   */
   private static buildForm(): FormGroup {
    return new FormGroup({
      title: new FormControl("", Validators.compose([Validators.required, Validators.minLength(2)])),
      description: new FormControl("", Validators.compose(null)),
      date: new FormControl("", Validators.compose([Validators.required])),
      artist: new FormControl("", Validators.compose([Validators.required])),
      album: new FormControl("", Validators.compose([Validators.required])),
      duration: new FormControl("", Validators.compose([Validators.required]))
    });
  }
}
