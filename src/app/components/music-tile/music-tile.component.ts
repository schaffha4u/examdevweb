import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Music } from 'src/app/models/music';

@Component({
  selector: 'app-music-tile',
  templateUrl: './music-tile.component.html',
  styleUrls: ['./music-tile.component.css']
})
export class MusicTileComponent {
  @Input() music!: Music;
  @Output() updateButtonEvent = new EventEmitter<void>();
  @Output() deleteButtonEvent = new EventEmitter<void>();

  constructor() {}

  openEditMusicDialog(){
    this.updateButtonEvent.emit();
  }

  openDeleteMusicDialog(){
    this.deleteButtonEvent.emit();
  }
}
