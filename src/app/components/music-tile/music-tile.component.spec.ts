import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MusicTileComponent } from './music-tile.component';

describe('ComponentsMusicTileComponent', () => {
  let component: MusicTileComponent;
  let fixture: ComponentFixture<MusicTileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MusicTileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MusicTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
