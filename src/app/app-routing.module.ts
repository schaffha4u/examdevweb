import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MusicsViewComponent } from './components/musics-view/musics-view.component';
import { MusicDetailsComponent } from './components/music-details/music-details.component';

const routes: Routes = [
  {
    path: "musics",
    component: MusicsViewComponent
  },
  {
    path: "music/:id",
    component: MusicDetailsComponent
  },
  {
    path: '**',
    redirectTo: 'musics'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
