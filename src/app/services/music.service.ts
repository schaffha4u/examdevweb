
import { Injectable } from '@angular/core';
import { API_URL_BASE } from '../shared/constants';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Music } from '../models/music';

@Injectable({
  providedIn: 'root'
})
export class MusicService {
  readonly API_URL: string = API_URL_BASE + "/musics";

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Music[]>{
    return this.http.get<Music[]>(this.API_URL);
  }

  public getRandom(): Observable<Music>{
    return this.http.get<Music>(`${this.API_URL}/random`);
  }

  public getById(id: string): Observable<Music>{
    return this.http.get<Music>(`${this.API_URL}/${id}`);
  }

  public searchByName(searchTerm: string): Observable<Music[]>{
    return this.http.get<Music[]>(`${this.API_URL}/title/${searchTerm}`);
  }

  public create(music: Music): Observable<any>{
    return this.http.post(this.API_URL, music);
  }

  public update(music: Music): Observable<any>{
    return this.http.put(`${this.API_URL}/${music.id}`, music);
  }

  public delete(id: string): Observable<any>{
    return this.http.delete(`${this.API_URL}/${id}`);
  }
}
